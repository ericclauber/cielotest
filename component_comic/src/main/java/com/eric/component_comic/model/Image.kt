package com.eric.component_comic.model

class Image(
    var path: String,
    val extension: String
)