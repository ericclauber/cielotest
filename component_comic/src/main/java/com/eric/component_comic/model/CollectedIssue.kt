package com.eric.component_comic.model

data class CollectedIssue(
    val resourceURI: String,
    val name: String
)