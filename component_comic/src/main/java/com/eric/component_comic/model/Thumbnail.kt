data class Thumbnail(

    var path: String,
    val extension: String
)
