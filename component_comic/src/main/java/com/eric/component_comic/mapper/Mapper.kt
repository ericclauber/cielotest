package com.eric.feature_comic.repository

import Comic
import Data

object Mapper {

    private val STANDARD_LARG_IMAGE_VARIANT = "standard_large"
    private val LANDSCAPE_INCREDIBLE_IMAGE_VARIANT = "landscape_incredible"

    fun mapperData(data: Data): Data {
            val dataMapper = data
        data.results.forEachIndexed { index, comic ->
            dataMapper
                .results[index]
                .thumbnail.path =
                "${comic.thumbnail.path}/$STANDARD_LARG_IMAGE_VARIANT.${comic.thumbnail.extension}"
        }
        return dataMapper
    }

    fun mapperComic(data: Data): Comic {
        val comic = data.results[0]
        comic.thumbnail.path =
            "${comic.thumbnail.path}/$LANDSCAPE_INCREDIBLE_IMAGE_VARIANT.${comic.thumbnail.extension}"
        return comic
    }
}