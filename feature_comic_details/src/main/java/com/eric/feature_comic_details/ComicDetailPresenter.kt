interface ComicDetailPresenter {

    fun onCreate(id: Int)
    fun onDestroy()
}