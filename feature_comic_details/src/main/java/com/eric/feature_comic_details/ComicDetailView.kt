package com.eric.feature_comic_details

import Comic

interface ComicDetailView {

    fun onComicSuccess(comic: Comic)
    fun onComicError(error: String?)
}