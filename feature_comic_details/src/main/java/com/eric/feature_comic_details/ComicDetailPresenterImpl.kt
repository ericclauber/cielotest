package com.eric.feature_comic_details

import ComicDetailPresenter
import com.eric.core.extensions.ioThread
import com.eric.feature_comic_details.repository.ComicDetailInteractor
import com.eric.feature_comic_details.repository.ComicDetailInteractorImpl
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class ComicDetailPresenterImpl(private val view: ComicDetailView) : ComicDetailPresenter {

    val interactor: ComicDetailInteractor = ComicDetailInteractorImpl()
    val disposible = CompositeDisposable()

    override fun onCreate(id: Int) {
        interactor.getComicDetail(id)
            .ioThread()
            .subscribe({
                view.onComicSuccess(it)
            }, {
                view.onComicError(it.message)
            })
            .addTo(disposible)
    }

    override fun onDestroy() {
        disposible.dispose()
    }
}