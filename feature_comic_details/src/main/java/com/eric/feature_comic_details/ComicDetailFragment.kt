package com.eric.feature_comic_details

import Comic
import ComicDetailPresenter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_comic_detail.*

class ComicDetailFragment : Fragment(), ComicDetailView {

    private val presenter: ComicDetailPresenter = ComicDetailPresenterImpl(this)
    private val TAG = this::class.java.simpleName

    companion object {
        private var comicDetailFragment: ComicDetailFragment? = null
        private val COMIC_ID = "COMIC_ID"

        fun getInstance(comicId: Int): ComicDetailFragment {
            if (comicDetailFragment == null) {
                comicDetailFragment = ComicDetailFragment()
                val extra = Bundle()
                extra.putInt(COMIC_ID, comicId)
                comicDetailFragment!!.arguments = extra
            }
            return comicDetailFragment!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return LayoutInflater.from(context)
            .inflate(R.layout.fragment_comic_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val comicId = arguments!!.getInt(COMIC_ID)
        presenter.onCreate(comicId)
    }

    override fun onComicSuccess(comic: Comic) {

        Picasso.with(context)
            .load(comic.thumbnail.path)
            .placeholder(R.drawable.load_image)
            .error(R.drawable.load_image_error)
            .into(imageViewComic)
    }

    override fun onComicError(error: String?) {
        Log.e("TAG", error)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}