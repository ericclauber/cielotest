package com.eric.feature_comic_details.repository

import Comic
import com.eric.feature_comic.repository.Mapper
import io.reactivex.Observable

class ComicDetailInteractorImpl : ComicDetailInteractor {

    val repository = ComicDetailRepository()
    override fun getComicDetail(id: Int): Observable<Comic> {
        return repository.getComicDetail(id).map { Mapper.mapperComic(it.data) }
    }
}