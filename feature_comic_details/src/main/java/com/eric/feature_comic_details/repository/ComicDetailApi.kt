package com.eric.feature_comic_details.repository

import Response
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ComicDetailApi {

    @Headers("Content-Type: appication/json")
    @GET("v1/public/comics/{code}")
    fun getComics(
        @Path("code") code: Int,
        @Query("ts") timestamp: String,
        @Query("apikey") apikey: String,
        @Query("hash") hash: String
    ): Observable<Response>
}