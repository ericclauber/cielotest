package com.eric.feature_comic_details.repository

import Response
import com.eric.core.BuildConfig
import com.eric.core.base.Keys
import com.eric.core.base.RetrofitBuilder
import io.reactivex.Observable

class ComicDetailRepository {

    private val comicDetailApi = RetrofitBuilder.createAPI(
        BuildConfig.URL_BASE,
        ComicDetailApi::class.java
    ) as ComicDetailApi

    fun getComicDetail(id: Int): Observable<Response> {
        return comicDetailApi
            .getComics(
                id,
                Keys.getTimeStamp(),
                Keys.getApiKey(),
                Keys.getHash()
            )
    }
}