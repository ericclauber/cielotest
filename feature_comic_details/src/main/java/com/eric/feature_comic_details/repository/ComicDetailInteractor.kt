package com.eric.feature_comic_details.repository

import Comic
import io.reactivex.Observable

interface ComicDetailInteractor {

    fun getComicDetail(id: Int): Observable<Comic>
}