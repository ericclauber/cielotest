package com.eric.cielotest

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.eric.core.extensions.addFragment
import com.eric.feature_comic.ComicActivity
import com.eric.feature_comic.ComicFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this, ComicActivity::class.java))
    }
}
