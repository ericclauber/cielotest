package com.eric.feature_comic

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.eric.core.extensions.addFragment

class ComicActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comic)
        this.addFragment(ComicFragment.getInstance(), R.id.container)
    }
}
