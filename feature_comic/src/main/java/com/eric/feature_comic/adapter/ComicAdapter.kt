package com.eric.feature_comic.adapter

import Comic
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eric.feature_comic.R

class ComicAdapter(
    val listener: onClickItem,
    val comicList: MutableList<Comic>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComicViewHolder {
        return LayoutInflater.from(parent.context)
            .inflate(R.layout.comic_item, parent, false)
            .let { ComicViewHolder(listener, parent.context, it) }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ComicViewHolder).bindItem(position, comicList[position])
    }

    override fun getItemCount() = comicList.size
    override fun getItemViewType(position: Int) = position

    interface onClickItem {
        fun onClickItem(comic: Comic)
    }
}