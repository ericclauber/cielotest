package com.eric.feature_comic.adapter

import Comic
import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.eric.core.extensions.toBRValue
import com.eric.feature_comic.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.comic_item.view.*


class ComicViewHolder(
    val listener: ComicAdapter.onClickItem,
    internal val context: Context,
    itemView: View
) :
    RecyclerView.ViewHolder(itemView) {

    fun bindItem(position: Int, item: Comic) {

        Picasso.with(context)
            .load(item.thumbnail.path)
            .placeholder(R.drawable.load_image)
            .error(R.drawable.load_image_error)
            .into(itemView.imageViewThumbnail)

        itemView.textViewTitle.text = item.title
        itemView.textViewPrice.text = item.prices[0].price.toBRValue()
        itemView.setOnClickListener {
            listener.onClickItem(item)
        }
    }
}