package com.eric.feature_comic

import com.eric.core.extensions.ioThread
import com.eric.feature_comic.repository.ComicApi
import com.eric.feature_comic.repository.ComicInteractorImpl
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class ComicPresenterImpl(
    private val comicView: ComicView
) : ComicPresenter {

    val LIMIT_PAGE = 20
    private val interactor = ComicInteractorImpl()
    private val disposible = CompositeDisposable()

    override fun onCreate(offset: Int) {
        interactor.getComics(LIMIT_PAGE, offset)
            .ioThread()
            .subscribe(
                { it?.let { comicView.onComicsSuccess(it) } },
                { comicView.onComicsError(it.message) })
            .addTo(disposible)
    }

    override fun onDetroy() {
        disposible.dispose()
    }
}