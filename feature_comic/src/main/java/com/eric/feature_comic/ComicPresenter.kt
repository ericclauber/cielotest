package com.eric.feature_comic

interface ComicPresenter {
    fun onCreate(offset: Int)
    fun onDetroy()
}