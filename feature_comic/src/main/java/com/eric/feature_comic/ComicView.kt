package com.eric.feature_comic

import Data

interface ComicView {

    fun onComicsSuccess(data: Data)
    fun onComicsError(error: String?)
}