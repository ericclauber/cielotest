package com.eric.feature_comic

import Comic
import Data
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eric.core.extensions.addFragment
import com.eric.core.utils.EndlessScrollListener
import com.eric.feature_comic.adapter.ComicAdapter
import com.eric.feature_comic_details.ComicDetailFragment
import kotlinx.android.synthetic.main.fragment_comic.*

class ComicFragment : Fragment(), ComicView, ComicAdapter.onClickItem {

    val TAG = this::class.java.simpleName
    private lateinit var adapter: ComicAdapter
    private var comicList: MutableList<Comic> = ArrayList()
    private val presenter: ComicPresenter = ComicPresenterImpl(this)
    private lateinit var scrollListener: EndlessScrollListener
    private val linearLayoutManager = LinearLayoutManager(
        context, LinearLayoutManager.VERTICAL, false
    )

    companion object {
        private var fragment: ComicFragment? = null
        fun getInstance(): ComicFragment {
            if (fragment == null) fragment = ComicFragment()
            return fragment!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_comic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        presenter.onCreate(0)
    }

    private fun initViews() {
        scrollListener =
            object : EndlessScrollListener(linearLayoutManager) {
                override fun onLoadMore(
                    page: Int,
                    totalItemsCount: Int,
                    view: RecyclerView?
                ) {
                    presenter.onCreate(page)
                }
            }
        adapter = ComicAdapter(this, comicList)
        comicReciclerView.layoutManager = linearLayoutManager
        comicReciclerView.addOnScrollListener(scrollListener)
        val decorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        comicReciclerView.addItemDecoration(decorator)
        comicReciclerView.adapter = adapter
    }

    override fun onComicsSuccess(data: Data) {
        comicList.addAll(data.results)
        adapter.notifyDataSetChanged()
    }

    override fun onComicsError(error: String?) {
        Log.e(TAG, error)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetroy()
    }

    override fun onClickItem(comic: Comic) {
        // TODO
    }
}