package com.eric.feature_comic.repository

import Data
import io.reactivex.Observable

interface ComicInteractor {

    fun getComics(limitePage: Int, offset: Int): Observable<Data>
}