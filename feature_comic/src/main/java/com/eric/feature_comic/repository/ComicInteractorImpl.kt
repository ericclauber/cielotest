package com.eric.feature_comic.repository

import Data
import io.reactivex.Observable

class ComicInteractorImpl : ComicInteractor {

    private val repository = ComicRepositoryImpl()

    override fun getComics(limitePage: Int, offset: Int): Observable<Data> {
        return repository.getComics(limitePage, offset).map { Mapper.mapperData(it.data) }
    }
}