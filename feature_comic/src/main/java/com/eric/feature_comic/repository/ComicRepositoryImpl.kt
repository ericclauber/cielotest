package com.eric.feature_comic.repository

import Response
import com.eric.core.BuildConfig
import com.eric.core.base.Keys
import com.eric.core.base.RetrofitBuilder
import io.reactivex.Observable

class ComicRepositoryImpl {

    private val comicApi = RetrofitBuilder.createAPI(
        BuildConfig.URL_BASE,
        ComicApi::class.java
    ) as ComicApi

    fun getComics(limitePage: Int, offset: Int): Observable<Response> {
        return comicApi.getComics(
            limitePage,
            offset,
            Keys.getTimeStamp(),
            Keys.getApiKey(),
            Keys.getHash()
        )
    }
}