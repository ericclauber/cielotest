package com.eric.feature_comic.di

import com.eric.feature_comic.ComicActivity
import com.eric.feature_comic.repository.ComicApi
import dagger.Component

@Component(modules = [RetrofitModule::class])
interface ComicComponent {

    fun provideComicApi(): ComicApi

    fun inject(comicActivity: ComicActivity)
}